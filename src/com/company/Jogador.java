package com.company;

public class Jogador extends Jogo {
    private String nome;
    private int idade;

    public Jogador(String nomeJogo, int rodada) {
        super(nomeJogo, rodada);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void sortear (String nome, int idade, int numFace, int aposta){
        Jogo jogo = new Jogo("Jogo de Dados", 1);
        jogo.jogar(nome, idade, numFace, aposta);
    }
}
