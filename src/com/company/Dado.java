package com.company;

import com.sun.org.apache.bcel.internal.generic.INEG;

import javax.jws.Oneway;
import java.util.ArrayList;
import java.util.Random;

public class Dado {

    private int[] faceDado = new int[6]; //atribuir valor padrão considerando requisição 1

    private  int numeroFace;

    public ArrayList<Integer> getListaFace() {
        return listaFace;
    }

    public void setListaFace(ArrayList<Integer> listaFace) {
        this.listaFace = listaFace;
    }

    ArrayList<Integer> listaFace = new ArrayList<Integer>();

    public Dado(int numeroFace) {
        ArrayList<Integer> gerador = new ArrayList<Integer>();
        for (int i= 1; i <= numeroFace; i++){
            gerador.add(i);
        }
        setListaFace(gerador);
    }

    public int getSorteado()
    {
        Random rd = new Random();
        return listaFace.get(rd.nextInt(listaFace.size()));
    }

    public int getTresSorteio(){
        Random rd = new Random();
        int modelo;
        modelo = listaFace.get(rd.nextInt(listaFace.size()));
        return modelo;
    }

    public int getNumeroFace() {
        return numeroFace;
    }

    public void setNumeroFace(int numeroFace) {
        this.numeroFace = numeroFace;
    }

    public int[] getFaceDado() {
        return faceDado;
    }

    @Override
    public String toString() {
        StringBuilder model = new StringBuilder();
        for (int n: this.listaFace) {
            model.append(n);
            model.append(", ");
        }

        return model.toString();
    }
}
