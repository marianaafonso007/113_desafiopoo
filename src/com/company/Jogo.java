package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Jogo {

    public Jogo(String nomeJogo, int rodada) {
        this.nomeJogo = nomeJogo;
        this.rodada = rodada;
    }

    private String nomeJogo;
    private int rodada;

    public String getNomeJogo() {
        return nomeJogo;
    }

    public void setNomeJogo(String nomeJogo) {
        this.nomeJogo = nomeJogo;
    }

    public int getRoda() {
        return rodada;
    }

    public void setRoda(int roda) {
        this.rodada = roda;
    }

    public int[][] sortearTresVezes(int numFace){
        Dado dado = new Dado(numFace);
        int[][] sorteiTres = new int[3][3];
        for (int j = 0; j < 3; j++){
            for (int i = 0; i < 3; i++){
                sorteiTres[j][i] = dado.getTresSorteio();
            }
        }
        return sorteiTres;
    }

    public void jogar(String nome, int idade, int numFace, int aposta){
        int[][] resultJogo = new int[3][3];
        resultJogo = sortearTresVezes(numFace);
        boolean acertou = false;
        int [] valorTresSorteios = new int[3];

        for(int j = 0; j < 3; j++){
            for (int i = 0; i < 3; i++){
                valorTresSorteios[j] += resultJogo[j][i];
                if (aposta == resultJogo[j][i]){
                    acertou = true;
                }
            }
        }

        if(acertou){
            System.out.println("Parabéns você acertou um dos números sorteados, sendo: " + aposta + " Segue a lista:");
        }else {
            System.out.println("Erroooou: sua aposta  " + aposta + " Segue a lista:");
        }
        for(int j = 0; j < 3; j++){
            for (int i = 0; i < 3; i++){
                System.out.print(" " + resultJogo[j][i] + " ");
            }
            System.out.println(" Valor da soma: "  + valorTresSorteios[j] + " ");
        }

    }
}
